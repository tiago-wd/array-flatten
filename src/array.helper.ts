import express from 'express';

export class ArrayFlatten {
  static flatten(_req: express.Request, res: express.Response) {
    res.send(ArrayFlatten.flatArray([1, 2, 3, [4, 5], 6, [7, [8, [9], 10]], 11]));
  }

  static flatArray(array: any[]) {
    let arrayFlatten: any[] = [];
    array.map((v: any) => {
      if (typeof v === "object") {
        arrayFlatten = arrayFlatten.concat(ArrayFlatten.flatArray(v));
      } else {
        arrayFlatten.push(v);
      }
    });

    return arrayFlatten;
  }
};
