import { ArrayFlatten } from './array.helper';
import request from 'supertest';
import app from './index';

describe('array-flatten', () => {

  it('flatten', () => {
    const array: any[] = [1, 2, 3, [4, 5], 6, [7, [8, [9], 10]], 11];
    expect(array.flat(5)).toStrictEqual(ArrayFlatten.flatArray(array));
  });

  it('/ (GET)', async () => {
    const arrayFlatten: any[] = [1, 2, 3, [4, 5], 6, [7, [8, [9], 10]], 11];
    await request(app).get("/").send(arrayFlatten).expect(arrayFlatten.flat(5));;
  });
});