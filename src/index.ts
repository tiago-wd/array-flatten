import express from "express";
import { ArrayFlatten } from './array.helper';

const app = express();

app.get('/', ArrayFlatten.flatten);

export default app;