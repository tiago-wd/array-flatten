import app from './index';

app.listen(4004, () => {
  console.log(`[SERVER] Running at http://localhost:4004`);
});